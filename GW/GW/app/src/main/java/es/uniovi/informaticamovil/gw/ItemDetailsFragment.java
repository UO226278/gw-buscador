package es.uniovi.informaticamovil.gw;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

public class ItemDetailsFragment extends Fragment {
    private static final String ITEM_ARG = "item";
    TextView name = null;
    TextView type = null;
    TextView description = null;
    TextView rarity = null;
    TextView level = null;
    TextView value = null;
    TextView peculiaridades = null;
    TextView gametypes = null;
    TextView restrictions = null;
    Item item = new Item();
    ImageView mImageView;
    String cadenaAux = "";

    Context contexto;

    Bitmap resultado;


    public static ItemDetailsFragment newInstance(Item it) {
        ItemDetailsFragment fragment = new ItemDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ITEM_ARG, it);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        View rootView;
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.item_details_fragment,
                container, false);


        contexto = getActivity().getApplicationContext();
            Bundle args = getArguments();
        if (args != null) {

            item = args.getParcelable(ITEM_ARG);
        }




        mImageView = (ImageView) rootView.findViewById(R.id.imageView);
        name = (TextView) rootView.findViewById(R.id.name);
        name.setText(item.getName());
        type = (TextView) rootView.findViewById(R.id.tipo2);
        type.setText(item.getType());
        description = (TextView) rootView.findViewById(R.id.descripcion2);
        description.setText(item.getDescription());
        description.setMovementMethod(new ScrollingMovementMethod());
        rarity = (TextView) rootView.findViewById(R.id.rareza2);
        rarity.setText(item.getRarity());
        level = (TextView) rootView.findViewById(R.id.nivel2);
        level.setText(item.getLevel()+"");
        value = (TextView) rootView.findViewById(R.id.valor2);
        value.setText(item.getVendor_value()+"");

        peculiaridades = (TextView) rootView.findViewById(R.id.peculiaridades2);
        peculiaridades.setMovementMethod(new ScrollingMovementMethod());

        for(String cadena : item.getFlags()){
            if(!cadenaAux.equals(""))
                cadenaAux = cadenaAux + ", " + cadena;
            else
                cadenaAux = cadenaAux + cadena;
        }
        peculiaridades.setText(cadenaAux);
        cadenaAux = "";

        gametypes = (TextView) rootView.findViewById(R.id.gametypes2);

        for(String cadena : item.getGame_types()){
            if(!cadenaAux.equals(""))
                cadenaAux = cadenaAux + ", " + cadena;
            else
                cadenaAux = cadenaAux + cadena;
        }
        gametypes.setText(cadenaAux);

        cadenaAux = "";
        restrictions = (TextView) rootView.findViewById(R.id.restrictions2);
        restrictions.setMovementMethod(new ScrollingMovementMethod());

        for(String cadena : item.getRestrictions()){
            if(!cadenaAux.equals(""))
                cadenaAux = cadenaAux + ", " + cadena;
            else
                cadenaAux = cadenaAux + cadena;
        }
        restrictions.setText(cadenaAux);


        if (savedInstanceState != null){
            resultado = savedInstanceState.getParcelable("imagen");
            mImageView.setImageBitmap(resultado);
        }
        else {
            if(ItemListFragment.checkConn(contexto)==true) {
                new DownloadImageTask(mImageView)
                        .execute(item.getIcon());
            }
        }




        return rootView;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("imagen", resultado);
    }



    class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            resultado = result;

        }
    }


}