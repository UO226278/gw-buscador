package es.uniovi.informaticamovil.gw;


import android.app.Activity;
import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import es.uniovi.informaticamovil.gw.R;

public class JSONParserItems extends Thread {

    private Activity activity;
    private JSONObject jObject;



    public ArrayList<Item> items = new ArrayList<Item>();

    private int item_id;

    private String icon;
    private String nombre;
    private String descripcion;
    private String rareza;
    private int nivel;
    private int value;
    String tipo;
    private ArrayList<String> auxFlags;
    private ArrayList<String> auxGameTypes;
    private ArrayList<String> auxRestrictions;
    Context contexto;

    String url2 = "https://api.guildwars2.com/v2/items/";
    //String urlIntermedia= "http://www.gw2shinies.com/api/idbyname/twilight";
    String url = "http://www.gw2shinies.com/api/json/idbyname/";
    //String url = "http://gw2tno.com/api/finditemidbyname/";


    public JSONParserItems( Context contexto, String texto){
        this.contexto = contexto;
        url = url + texto;

    }

    public String getNombre (){
        return nombre;
    }




    public ArrayList<Item> readJSONItems() throws JSONException, NullPointerException{


        JSONArray array = JSONManager.getJSONfromURLArray(url);
        JSONArray arrayAux;
        ArrayList<Item> items = new ArrayList<Item>();
        if(array != null){


            //Limitación del número de items en el listview
            int longitudItems=15;
            if (array.length() < longitudItems) {
                longitudItems = array.length();
            }


            for(int i = 0; i < longitudItems; i++) {
                jObject = array.getJSONObject(i);
                nombre = jObject.getString("name");
                item_id = jObject.getInt("item_id");

                jObject = JSONManager.getJSONfromURLObject(url2 + item_id);
                nombre = jObject.getString("name");
                if(jObject.has("description")){
                    descripcion = jObject.getString("description");
                }
                else{
                    descripcion= contexto.getResources().getString(R.string.no_description);
                }


                nivel = jObject.getInt("level");
                rareza = jObject.getString("rarity");
                icon = jObject.getString("icon");
                tipo = jObject.getString("type");
                value = jObject.getInt("vendor_value");


                auxFlags = new ArrayList<>();
                arrayAux = jObject.getJSONArray("flags");
                if(arrayAux != null) {

                    for(int j = 0; j < arrayAux.length(); j++) {

                        auxFlags.add(arrayAux.get(j).toString());


                    }
                }
                auxGameTypes = new ArrayList<>();
                arrayAux = jObject.getJSONArray("game_types");
                if(arrayAux != null) {

                    for(int j = 0; j < arrayAux.length(); j++) {

                        auxGameTypes.add(arrayAux.get(j).toString());

                    }
                }
                auxRestrictions = new ArrayList<>();
                arrayAux = jObject.getJSONArray("restrictions");
                if(arrayAux != null) {

                    for(int j = 0; j < arrayAux.length(); j++) {
                        auxRestrictions.add(arrayAux.get(j).toString());

                    }
                }
                items.add(new Item(nombre, descripcion, nivel, rareza, icon, tipo, value, auxFlags, auxGameTypes,auxRestrictions));

            }

        }

        return items;
    }



}
