package es.uniovi.informaticamovil.gw;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import es.uniovi.informaticamovil.gw.R;


public class MainActivity extends ActionBarActivity implements

        ItemListFragment.Callbacks  {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        if(savedInstanceState == null) {
            FragmentManager FrgMng = getSupportFragmentManager();
            FragmentTransaction Tran = FrgMng.beginTransaction();
            Tran.add(R.id.course_list_frag, ItemListFragment.newInstance(), "tag");
            Tran.commit();
        }

    }


    @Override
    public void onItemSelected(Item item) {

        Intent intent = new Intent(this, ItemDetailsActivity.class);

        intent.putExtra("item",
                item);
        startActivity(intent);

    }

    //HACE QUE SI LE DAS A BACK NO SE DESTRUYA LA ACTIVIDAD Y OCURRA LO MISMO QUE AL DAR A HOME
    public void onBackPressed() {
        moveTaskToBack(true);
    }

}
