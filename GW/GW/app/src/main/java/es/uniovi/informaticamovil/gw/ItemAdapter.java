package es.uniovi.informaticamovil.gw;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ItemAdapter extends BaseAdapter {
	
	static class ViewHolder {
		public TextView mName;
		public TextView mDescription;


	}
	
	private final List<Item> mItems;
	public LayoutInflater mInflater;

	
	public ItemAdapter(Context context, List<Item> items) {

		if (context == null || items == null ) {
			throw new IllegalArgumentException();
		}
			
		this.mItems = items;
		this.mInflater = LayoutInflater.from(context);
	}
		
	@Override
	public int getCount() {

		return mItems.size();
	}

	@Override
	public Object getItem(int position) {
		
		return mItems.get(position);
	}

	@Override
	public long getItemId(int position) {

		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        Item item = (Item) getItem(position);
		View rowView = convertView;
		ViewHolder viewHolder;
		if (rowView == null) {
			rowView = mInflater.inflate(R.layout.list_item2, parent, false);
			viewHolder = new ViewHolder();


			viewHolder.mName = (TextView) rowView.findViewById(R.id.nameTextView);
			viewHolder.mDescription = (TextView) rowView.findViewById(R.id.descriptionTextView);
			rowView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) rowView.getTag();
		}

		viewHolder.mName.setText(item.getName());
		viewHolder.mDescription.setText(item.getDescription());

		
		return rowView;
	}
	
	public void addItem(Item item) {
		
		if (item == null) {
			throw new IllegalArgumentException();			
		}
		
		mItems.add(item);
		
		// Importante: notificar que ha cambiado el dataset
		notifyDataSetChanged();
	}

    public void clear() {
        mItems.clear();
        // Importante: notificar que ha cambiado el dataset
        notifyDataSetChanged();
    }



}
